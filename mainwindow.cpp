#include "mainwindow.h"
#include "ui_mainwindow.h"
//int HHT_PLATFORM_INFO = PLATFORM_DEFAULT;
int HHT_PLATFORM_INFO = PLATFORM_MSD828_15;
QStringList  gProgectList;
QString        gPlatfrom;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initVariables();
    initWnd();
#if 0
    mGetUsbDiskWidget = new GetUsbDiskWidget();
    connect(mGetUsbDiskWidget,SIGNAL(select_disk_signals(QString)),this,SLOT(select_disk_slot(QString)));
    mGetUsbDiskWidget->show();
#endif
}

MainWindow::~MainWindow()
{
    delete ui;
}
/**
 * @brief MainWindow::CompareList
 * @param src
 * @param dest
 * @return
 * 比较两个QStringList是否完全一致
 */
bool MainWindow::CompareList(QStringList src, QStringList dest)
{
    bool result =false;
    int mCount =src.length(),index=0;
    if(src.length()!=dest.length())
        return result;
    for(int i=0;i<mCount;i++)
    {
        for(int j=0;j<dest.length();j++)
        {
            QString filePath = dest.at(i);
            if(filePath.contains(src.at(i)))
            {
                index++;
                break;
            }
        }
    }
    qDebug()<<"+++++"<<mCount<<"########"<<index;
    if(index==mCount)
    {
        result =true;
    }
    else
    {
        result =false;
    }
    return result;
}
/**
 * @brief MainWindow::dirFileSize
 * @param path
 * @return
 * 获取整个目标文件夹大小
 */
quint64 MainWindow::dirFileSize(const QString &path)
{
    QDir dir(path);
    quint64 size = 0;
    foreach (QFileInfo fileInfo, dir.entryInfoList(QDir::Files))
    {
        size +=fileInfo.size();
    }
    foreach (QString subDir, dir.entryList(QDir::Dirs|QDir::NoDotAndDotDot))
    {
        size +=dirFileSize(path+QDir::separator()+subDir);
    }
    QString unit ="B";
    quint64 showSize = size;
    if(showSize>1024.0)
    {
        showSize /=1024.0;
        unit  ="K";
        if(showSize>1024.0)
        {
            showSize /=1024.0;
            unit ="M";
            if(showSize>1024.0)
            {
                showSize /=1024.0;
                unit ="G";
            }
        }
    }
    //qDebug()<<"文件夹大小: "<<showSize<<unit;
    return size;
}
/**
 * @brief MainWindow::quazip_test
 * quazip测试
 */
void MainWindow::quazip_test()
{
#if 1
    const QString srcPath = "C:/Users/jhonconal/Desktop/HHT-VCTL";
    const QString srcCompare = srcPath+".zip";
    const QString destPath = srcPath+"-extract";
    quint64 size = dirFileSize(srcPath);
    qDebug()<<"+++++"<<size<<" B";
    //压缩 ZIP
    bool result =JlCompress::compressDir(srcCompare,srcPath);
    if(!result)
    {
        qDebug()<<"compress failed.";
    }
    else
    {
        qDebug()<<"compress success.";
    }
    //获取压缩ZIP包含的文件
    QStringList src = JlCompress::getFileList(srcCompare);
    for(int i=0;i<src.length();i++)
    {
        qDebug()<<src.at(i);
    }
    //解压缩ZIP 返回解压缩获取的文件列表
    QStringList dest=JlCompress::extractDir(srcCompare,destPath);
    for(int i=0;i<dest.length();i++)
    {
        qDebug()<<dest.at(i);
    }
    //对比压缩与解压缩包含文件列表,判断解压缩成功
    bool rest= CompareList(src,dest);
    if(rest)
    {
        qDebug()<<"extract zip success.";
    }
#endif
}

bool MainWindow::compressDirEncrypt(QString fileCompressed, QString dir, bool isEncrypt)
{
    QDir srcDir(dir);
    if(!srcDir.exists())
        return false;
    if(isEncrypt)//是否加密
    {
        qDebug()<<"Try to encrypt. ";
        bool  result = JlCompress::compressDir(fileCompressed,dir);
        if(result)
            return true;
        else
            return false;
#if 1
        QuaZipFile zipFile(fileCompressed);
        QuaZipNewInfo newInfo(ENCRYPT_PASSWD);
        QByteArray bytearray;
        bytearray.append(ENCRYPT_PASSWD);
        QByteArray hash = QCryptographicHash::hash(bytearray,QCryptographicHash::Md5);
        QString  md5Hash = hash.toHex().toLower();

        bool ret = zipFile.open(QIODevice::WriteOnly,newInfo,
                                md5Hash.toUtf8().constData()
                                ,0          //CRC值 默认为0
                                ,8);        //写入方法 0为文件夹 8为普通文件
        if(ret)
        {
            QFile encryptZipFile(fileCompressed);
            if(encryptZipFile.open(QIODevice::ReadOnly))
            {
                zipFile.write(encryptZipFile.readAll());
            }
            encryptZipFile.close();
            zipFile.close();
            return true;
        }
#endif
    }
    else
    {
        qDebug()<<"Do not encrypt. ";
        bool  result = JlCompress::compressDir(fileCompressed,dir);
        if(result)
            return true;
        else
            return false;
    }
    return true;
}


/**
 * @brief MainWindow::GenerateJSon
 * @return  返回JSon文件
 */
QString MainWindow::GenerateJSon()
{
    QString generateJsonPath;


    return generateJsonPath;
}
/**
 * @brief MainWindow::AnalyticsJSon
 * @param jsonPath
 * 传入待解析Json文件
 */
void MainWindow::AnalyticsJSon(QString jsonPath)
{

    QFile jsonFile;
    jsonFile.setFileName(jsonPath);
    if(!jsonFile.open(QIODevice::ReadWrite))
    {
        qDebug()<<"read json file failed.";
        return;
    }
    QByteArray ba = jsonFile.readAll();
    qDebug()<<"++++++++++++++++++++++++++++++++++";
    qDebug()<<ba;
    qDebug()<<"++++++++++++++++++++++++++++++++++";
    QJsonParseError jsonError;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(ba,&jsonError);
    if(jsonError.error==QJsonParseError::NoError&&!jsonDocument.isNull())
    {
        if(jsonDocument.isObject()) //JSON
        {
            //TODO操作处理读取到的json数据
        }
    }
}

void MainWindow::ParseXML(QString xmlFilePath)
{
    if(xmlFilePath.isEmpty())
        return;
    QFile xml(xmlFilePath);
    if(!xml.open(QFile::ReadOnly|QFile::Text))
    {
        qDebug()<<tr("Open XML(%1) file failed.").arg(xmlFilePath);
        return;
    }
    QDomDocument xmlDocument;
    QString error;
    int row =0,colum=0;
    if(!xmlDocument.setContent(&xml,false,&error,&row,&colum))
    {
        qDebug()<<"========"<<error;
        return ;
    }
    if(xmlDocument.isNull())
    {
        qDebug()<<"XML is null. ";
        return ;
    }

    QDomElement root = xmlDocument.documentElement();
    QString root_tag = root.tagName(); //Configuration-properties
    QString root_tag_value;
    if(root.hasAttribute(tr("name")))
    {
        root_tag_value = root.attributeNode(tr("name")).value();
        qDebug()<<tr("<%1 name=%2>").arg(root_tag).arg(root_tag_value);
    }

#if 0
    QDomElement element_item = root.firstChildElement();
    while (!element_item.isNull())
    {
        qDebug()<<"tagName: "<<element_item.tagName();
        qDebug()<<"tagValue:"<<element_item.text();
        element_item = element_item.nextSiblingElement();
    }
#else
    QDomElement element_item = root.firstChildElement();
    if(element_item.isNull())
    {
        qDebug()<<"No child element.";
        return;
    }
    QDomNodeList nodeList = root.childNodes();
    int count = nodeList.count();
    mNodeListCount = count;
    qDebug()<<"#######NodeList count: "<<count;
    for(int i=0;i<count;i++)
    {
        QDomNode dom_node = nodeList.item(i);
        QDomElement element = dom_node.toElement();

        //获取id值，等价
        QString id_1 = element.attributeNode("id").value();
        QString id_2 = element.attribute("id");
        qDebug()<<"++++++"<<id_1<<"+++++"<<id_2;

        //获取子节点，数目为4，包括：name、property、value
        QDomNodeList child_list = element.childNodes();
        int child_count = child_list.count();
        int child_index =1;
        QStringList itemInfoList;
        QFont font,font2,font3;
        font.setFamily(tr("Century Schoolbook"));
        font2.setFamily(tr("Times"));
        font.setPointSize(14);
        font2.setPointSize(13);
        font.setBold(true);
        for(int j=0; j<child_count;j++)
        {
            QDomNode child_dom_node = child_list.item(j);
            QDomElement child_element = child_dom_node.toElement();
            QString child_tag_name = child_element.tagName();
            QString child_tag_value = child_element.text();
            qDebug()<<">>>>>>"<<child_tag_value;
            qDebug()<<QString("<%1>%2</%3>").arg(child_tag_name).arg(child_tag_value).arg(child_tag_name);
            //TODO更新显示列表
#if 1
            qDebug()<<"+++++++++++++++++"<<child_index;
            itemInfoList<<child_tag_value;
            if(child_index==3)
            {
                //                for(int i=0;i<itemInfoList.length();i++)
                //                {
                //                    qDebug()<<"//////////"<<itemInfoList.at(i);
                //                }
                QTreeWidgetItem *item =new QTreeWidgetItem(ui->treeWidget);
                item->setFont(0,font);
                item->setFont(2,font);
                item->setText(0,tr("%1").arg(itemInfoList.at(0)));
                qDebug()<<"#############"<<itemInfoList.at(0);
#if HHT_PROPERTY_VISABLE
                item->setText(1,tr("prop %1").arg(itemInfoList.at(1)));
#else
                font3.setFamily(tr("Times"));
                font3.setPointSize(10);
                item->setFont(1,font3);
                item->setToolTip(1,tr("prop %1").arg(itemInfoList.at(1)));
                ////item->setToolTip(1,tr("%1").arg(itemInfoList.at(1)));
                item->setText(1,tr("PROPERTY NOT VISABLE"));
#endif
                item->setText(2,tr("%1").arg(itemInfoList.at(2)));
#if 0
                //                if((item->text(0)=="customer.id")||(item->text(1)=="persist.hht.customer.id"))
                //                {
                //                    QString datetime = QDateTime::currentDateTime().toString("yyyyMMdd");
                //                    QString persist_value = itemInfoList.at(2)+"_"+datetime;
                //                    item->setText(2,tr("%1").arg(persist_value));
                //                }
#endif
                item->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled|Qt::ItemIsSelectable);
                if(itemInfoList.at(2)==tr("true"))
                {
                    item->setCheckState(0,Qt::Checked);
                }
                else if(itemInfoList.at(2)==tr("false"))
                {
                    item->setCheckState(0,Qt::Unchecked);
                }
                else
                {
                    qDebug()<<"Do nothing...";
                }
                itemInfoList.clear();
                child_index=0;
            }
            child_index++;
#endif
        }
    }
#endif
    xml.close();
}

bool MainWindow::WriteXMLContent(QString updatePropPath)
{
    QDir propDir(updatePropPath);
    if(!propDir.exists())
    {
        qDebug()<<QString("Arlem: (%1) Not Exist.").arg(updatePropPath);
        bool result = propDir.mkpath(updatePropPath);
        qDebug()<<"++++++++"<<result;
        if(!result)
        {
            qDebug()<<tr("创建%1 failed.").arg(updatePropPath);
            QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("创建文件夹%1失败....").arg(updatePropPath));
            QMessageBox::warning(NULL,"HHT-VCTL",text);
            return false;
        }
        qDebug()<<tr("创建 %1 文件夹success.").arg(updatePropPath);
    }
    QString propFilePath = updatePropPath+"/updateprop.txt";
    QFile propFile(propFilePath);
    if(!propFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        qDebug()<<tr("open %1 failed.").arg(propFilePath);
        QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("创建文件(%1)失败....").arg("updateprop.txt"));
        QMessageBox::warning(NULL,"HHT-VCTL",text);
        return false;
    }
    else
    {
        QTextStream out(&propFile);
        QTreeWidgetItemIterator it(ui->treeWidget);
        while (*it)
        {
            QString  treeValue;
            if(((*it)->text(0)=="customer.id")||
                    ((*it)->text(1)=="persist.hht.customer.id"))
            {
#if  0
                QString datetime = QDateTime::currentDateTime().toString("yyyyMMddhhmm");
                treeValue=(*it)->text(2)+"_"+datetime; //为改属性追加时间
#else
                //判断HOME/.commit_id.txt是否存在
                QString commit_file = mExcutePath+"/.commit_id.txt";
                QFile file(commit_file);
                bool isOpen=file.open(QIODevice::ReadOnly|QIODevice::Text);
                if(isOpen)
                {
                    QByteArray ba = file.readLine();//制度去short commit_id
                    QString commit_id = QString(ba).remove('\n');
                    qDebug()<<"###Get commit id for commit_id.txt : "<<commit_id;
                    if(!commit_id.isNull())
                    {
                        QString datetime = QDateTime::currentDateTime().toString("yyyyMMddhhmm");
                        treeValue=(*it)->text(2)+"_"+datetime+"_"+commit_id; //为改属性追加时间+git commit id
                        qDebug()<<"persist.customer.id: "<<treeValue;
                    }
                    else
                    {
                        QString datetime = QDateTime::currentDateTime().toString("yyyyMMddhhmm");
                        treeValue=(*it)->text(2)+"_"+datetime; //为改属性追加时间
                    }
                }
                else
                {
                    QString datetime = QDateTime::currentDateTime().toString("yyyyMMddhhmm");
                    treeValue=(*it)->text(2)+"_"+datetime; //为改属性追加时间
                }
                file.close();
#endif
            }
            else
            {
                treeValue = (*it)->text(2);
            }
#if  HHT_PROPERTY_VISABLE
            //qDebug()<<"#####"<<(*it)->text(1)<<"="<<(*it)->text(2);
            out<<(*it)->text(1)+"="+treeValue+"\n";
#else
            out<<(*it)->toolTip(1)+"="+treeValue+"\n";
            /////out<<(*it)->toolTip(1)+"="+(*it)->text(2)+"\n";
#endif
            ++it;
        }
    }
    propFile.close();
    return true;
}

QStringList MainWindow::getFileNames(const QString &path,const QStringList &nameFilters)
{
    QDir dir(path);
    QStringList files = dir.entryList(nameFilters, QDir::Files|QDir::Readable, QDir::Name);
    return files;
}

QStringList MainWindow::getFileNames(const QString &path)
{
    QDir dir(path);
    QStringList nameFilters;
    //    nameFilters << "*.json";
    nameFilters << "*.xml";
    QStringList fileNameList = dir.entryList(nameFilters, QDir::Files|QDir::Readable, QDir::Name);
    return fileNameList;
}

bool MainWindow::copyFileToPath(QString sourceDir, QString toDir, bool coverFileIfExist)
{
    //    toDir.replace("\\","/");
    if (sourceDir == toDir){
        qDebug()<<"1111111";
        return true;
    }
    if (!QFile::exists(sourceDir)){
        qDebug()<<"222222";
        return false;
    }
    QDir *createfile     = new QDir;
    bool exist = createfile->exists(toDir);
    if (exist){
        if(coverFileIfExist){
            createfile->remove(toDir);
        }
    }//end if

    if(!QFile::copy(sourceDir, toDir))
    {
        qDebug()<<"33333";
        return false;
    }
    qDebug()<<"44444";
    return true;
}

bool MainWindow::copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist)
{
    QDir sourceDir(fromDir);
    QDir targetDir(toDir);
    if(!targetDir.exists()){    /**< 如果目标目录不存在，则进行创建 */
        if(!targetDir.mkdir(targetDir.absolutePath()))
            return false;
    }

    QFileInfoList fileInfoList = sourceDir.entryInfoList();
    foreach(QFileInfo fileInfo, fileInfoList){
        if(fileInfo.fileName() == "." || fileInfo.fileName() == "..")
            continue;

        if(fileInfo.isDir()){    /**< 当为目录时，递归的进行copy */
            if(!copyDirectoryFiles(fileInfo.filePath(),
                                   targetDir.filePath(fileInfo.fileName()),
                                   coverFileIfExist))
                return false;
        }
        else{            /**< 当允许覆盖操作时，将旧文件进行删除操作 */
            if(coverFileIfExist && targetDir.exists(fileInfo.fileName())){
                targetDir.remove(fileInfo.fileName());
            }

            /// 进行文件copy
            if(!QFile::copy(fileInfo.filePath(),
                            targetDir.filePath(fileInfo.fileName()))){
                return false;
            }
        }
    }
    return true;
}

bool MainWindow::delDir(const QString &path)
{
    if (path.isEmpty()){
        return false;
    }
    QDir dir(path);
    if(!dir.exists()){
        return true;
    }
    dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot); //设置过滤
    QFileInfoList fileList = dir.entryInfoList(); // 获取所有的文件信息
    foreach (QFileInfo file, fileList){ //遍历文件信息
        if (file.isFile()){ // 是文件，删除
            file.dir().remove(file.fileName());
        }else{ // 递归删除
            delDir(file.absoluteFilePath());
        }
    }
    return dir.rmpath(dir.absolutePath()); // 删除文件夹
}

char *MainWindow::GetBuildDate()
{
#define YEAR ((((__DATE__ [7] - '0') * 10 + (__DATE__ [8] - '0')) * 10 \
    + (__DATE__ [9] - '0')) * 10 + (__DATE__ [10] - '0'))
    //Jan  Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
#define MONTH (__DATE__ [2] == 'n' ? (__DATE__ [1] == 'a' ? 0 : 5) \
    : __DATE__ [2] == 'b' ? 1 \
    : __DATE__ [2] == 'r' ? (__DATE__ [0] == 'M' ? 2 : 3) \
    : __DATE__ [2] == 'y' ? 4 \
    : __DATE__ [2] == 'n' ?(__DATE__ [1] == 'u' ? 5 : 0) \
    : __DATE__ [2] == 'l' ? 6 \
    : __DATE__ [2] == 'g' ? 7 \
    : __DATE__ [2] == 'p' ? 8 \
    : __DATE__ [2] == 't' ? 9 \
    : __DATE__ [2] == 'v' ? 10 : 11)

#define DAY ((__DATE__ [4] == ' ' ? 0 : __DATE__ [4] - '0') * 10 \
    + (__DATE__ [5] - '0'))

#define DATE_AS_INT (((YEAR - 2000) * 12 + MONTH) * 31 + DAY)
    qDebug("####%s %s",__DATE__,__TIME__);
    //LOGD_STRING("version:"+StringUtils::float2str(VERIONS_CODE)+__DATE__);
    static char buildtime[256]={0};
    //sprintf(buildtime,"build time:%d-%02d-%02d",YEAR, MONTH + 1, DAY);
    sprintf(buildtime,"%d-%02d-%02d %s",YEAR, MONTH + 1, DAY,__TIME__);
    // printf("%s\n",buildtime);
    return buildtime;
}

QString MainWindow::GetBuildVersion()
{
    QString buildVersion;
    QString strTime =QLatin1Literal(GetBuildDate());
    qDebug()<<"++++"<<strTime;
    buildVersion = MAJOR_VERSION+strTime.mid(2,2)+"."+strTime.mid(5,2)+"."+strTime.mid(8,2);
    return buildVersion;
}

void MainWindow::initWnd()
{

    ui->pannelBox->setView(new QListView);
    ui->pannelBox->setStyleSheet("QComboBox{min-height: 35px;  background-color: rgb(220,220,220);"
                                 "color: black; border-radius: 3px; padding: 1px 5px 1px 3px;} "
                                 "QComboBox::drop-down{width: 10px;background-color: rgb(0, 122, 204,150) }"
                                 "QComboBox QAbstractItemView{background-color: rgb(255,255,255);color:black;font-size:18px;}"
                                 "QComboBox QAbstractItemView:item{min-height: 35px; }");
    ui->projectBox->setView(new QListView);
    ui->projectBox->setStyleSheet("QComboBox{min-height: 35px;  background-color: rgb(220,220,220);"
                                  "color: black; border-radius: 3px; padding: 1px 5px 1px 3px;} "
                                  "QComboBox::drop-down{width: 10px;background-color: rgb(0, 122, 204,150) }"
                                  "QComboBox QAbstractItemView{background-color: rgb(255,255,255);color:black;font-size:18px;}"
                                  "QComboBox QAbstractItemView:item{min-height: 35px; }");

    ui->comboBox->setView(new QListView);
    ui->comboBox->setStyleSheet("QComboBox{min-height: 35px;  background-color: rgb(220,220,220);"
                                "color: black; border-radius: 3px; padding: 1px 5px 1px 3px;} "
                                "QComboBox::drop-down{width: 10px;background-color: rgb(0, 122, 204,150) }"
                                "QComboBox QAbstractItemView{background-color: rgb(255,255,255);color:black;font-size:18px;}"
                                "QComboBox QAbstractItemView:item{min-height: 35px; }");
#if 0
    if(HHT_PLATFORM_INFO)
    {
        ui->platformLabel->setText(gPlatfrom);
        ui->comboBox->addItems(gProgectList);
    }
    else
    {
        ui->widget->setVisible(false);
    }
#else
    if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
        ui->widget->setVisible(false);
    else {
        ui->platformLabel->setText(gPlatfrom);
        ui->comboBox->addItems(gProgectList);
    }
#endif
    setWindowFlags(this->windowFlags()
                   &~Qt::WindowMaximizeButtonHint);//窗口置顶 隐藏最大最小按钮
    ////初始化treewidget
    ui->treeWidget->setColumnCount(3);
    ui->treeWidget->clear();
    QStringList textList;
    textList<<"Name"<<"Property"<<"Value";
    ui->treeWidget->setHeaderLabels(textList);
    ui->treeWidget->setStyleSheet("QTreeView::item{margin:8px;}");
    QHeaderView *head = ui->treeWidget->header();
    head->setSectionResizeMode(QHeaderView::ResizeToContents);

#if 0
    ui->listWidget->setResizeMode(QListView::Adjust);   // 设置大小模式-可调节
    ui->listWidget->setViewMode(QListView::ListMode);   // 设置显示模式
    ui->listWidget->setMovement(QListView::Static);     // 设置单元项不可被拖动
    for(int i =0;i<12;i++)
    {
        QListWidgetItem *item = new QListWidgetItem();
        QString text = tr("item: %1").arg(i);
        item->setText(text);
        item->setCheckState(Qt::Checked);
        ui->listWidget->addItem(item);
    }
#endif
    on_actionrefresh_triggered();
}

void MainWindow::initVariables()
{
    isZipOutput = false;
    mExcutePath = qApp->applicationDirPath();
    qDebug()<<"++++++"<<mExcutePath;
    ////connect(ui->actionabout,SIGNAL(triggered()),this,SLOT(on_actionabout_triggered()));
    ////connect(ui->actionopen,SIGNAL(triggered()),this,SLOT(on_actionopen_triggered()));
    ////connect(ui->actionsave,SIGNAL(triggered()),this,SLOT(on_actionsave_triggered()));
    ////connect(ui->actionsave_as,SIGNAL(triggered()),this,SLOT(on_actionsave_as_triggered()));
    ////connect(ui->actionrefresh,SIGNAL(triggered()),this,SLOT(on_actionrefresh_triggered()));
    ////connect(ui->actionoutput,SIGNAL(triggered()),this,SLOT(on_actionoutput_triggered()));
    QString strTime = QString::fromLocal8Bit(__DATE__) + "  " + QString::fromLocal8Bit(__TIME__);
    QString version = "Version : "+ GetBuildVersion();
    mAbout= QString("<font face='Impact' size='6' bold='true' color='black'>%1</font> <br>Update\t: %2 build</br><br>%3</br>")
            .arg(version).arg(strTime).arg(tr("Copyright @ 2018 - 2019 Hitevision. All Rights Reserved."));
    ui->actionsave_as->setVisible(false);
    ui->actionopen->setVisible(false);
}

void MainWindow::on_actionopen_triggered()
{

}

void MainWindow::on_actionsave_triggered()
{
    on_generateButton_clicked();
}

void MainWindow::on_actionsave_as_triggered()
{

}

void MainWindow::on_actionabout_triggered()
{
    QMessageBox::about(this,"HHT-VCTL",mAbout);
}

void MainWindow::on_actionrefresh_triggered()
{
    ui->pannelBox->clear();
    ui->projectBox->clear();
    //初始化combox
    QString ConfiguartionPath = mExcutePath+"/hht-vctls";
    QDir homeDir(ConfiguartionPath);
    if(homeDir.exists())
    {
        if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
        {
            qDebug()<<"PLATFORM_MSD828_DEFAULT";

            mConfiguartionPath =ConfiguartionPath;
            qDebug()<<"++++"<<mConfiguartionPath;
            mPanelPath = mConfiguartionPath+tr("/屏参");
            mDevicePath = mConfiguartionPath+tr("/项目");
            //////HHT-VCTL/hht-vctls/屏参/*.zip
            QDir panelDir(mPanelPath);
            QDir deviceDir(mDevicePath);
            if(!panelDir.exists())
            {
                QString panel = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("\"hht-vctls/屏参\" 文件夹不存在...."));
                QMessageBox::warning(NULL,"HHT-VCTL",panel);
            }
            else
            {
                qDebug()<<"++++"<<mPanelPath;
                QStringList nameFilters;
                nameFilters<<"*.zip";
                QStringList panelList = getFileNames(mPanelPath,nameFilters);
                if(!panelList.isEmpty())
                {
                    ui->pannelBox->addItems(panelList);
                }
                mPanelFilePath = mPanelPath+"/"+ui->pannelBox->currentText();
                //            qDebug()<<"1.###########"<<mPanelFilePath;
            }

            if(!deviceDir.exists())
            {
                QString device = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("\"hht-vctls/项目\" 文件夹不存在...."));
                QMessageBox::warning(NULL,"HHT-VCTL",device);
            }
            else
            {
                QStringList deviceList = getFileNames(mDevicePath);

                if(!deviceList.isEmpty())
                {
                    ui->projectBox->addItems(deviceList);
                }
                mDeviceFilePath = mDevicePath+"/"+ui->projectBox->itemText(0);
                //            qDebug()<<"2.###########"<<mDeviceFilePath;
            }
        }//end if(HHT_PLATFORM_INFO!=PLATFORM_MSD828_15)
        else
        {
            //读取配置文件为MSD828.15平台时
            ////TODO
            qDebug()<<"PLATFORM_MSD8(2/4)8_15";
            mConfiguartionPath =ConfiguartionPath;
            QString  projectsPath=ConfiguartionPath+"/"+ui->comboBox->currentText();
            mProjectPath = projectsPath;
            QDir projectsDir(mProjectPath);
            if(projectsDir.exists())
            {
                QStringList nameFilters;
                nameFilters<<"*.zip";
                QStringList panelList = getFileNames(mProjectPath,nameFilters);
                if(!panelList.isEmpty())
                {
                    ui->pannelBox->addItems(panelList);
                }
                mPanelFilePath = mProjectPath+"/"+ui->pannelBox->itemText(0);

                QStringList Filters;
                Filters<<"*.xml";
                QStringList deviceList = getFileNames(mProjectPath,Filters);

                if(!deviceList.isEmpty())
                {
                    ui->projectBox->addItems(deviceList);
                }
                mDeviceFilePath = mProjectPath+"/"+ui->projectBox->itemText(0);
            }
        }

    }
    else {
        QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("\"hht-vctls\" 家目录配置文件夹不存在...."));
        QMessageBox::warning(NULL,"HHT-VCTL",text);
    }
}


#if 0
//#ifdef Q_OS_WIN32
char MainWindow::FirstDriveFromMask(ULONG unitmask)
{
    char i;
    for (i = 0; i < 26; ++i)
    {
        if (unitmask & 0x1)
            break;
        unitmask = unitmask >> 1;
    }
    return (i + 'A');
}
bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    Q_UNUSED(eventType);
    Q_UNUSED(result);
    MSG* msg = reinterpret_cast<MSG*>(message);
    int msgType = msg->message;
    if(msgType==WM_DEVICECHANGE)
    {
        PDEV_BROADCAST_HDR lpdb = (PDEV_BROADCAST_HDR)msg->lParam;
        switch (msg->wParam) {
        case DBT_DEVICEARRIVAL:
            if(lpdb->dbch_devicetype == DBT_DEVTYP_VOLUME)
            {
                PDEV_BROADCAST_VOLUME lpdbv = (PDEV_BROADCAST_VOLUME)lpdb;
                if(lpdbv->dbcv_flags ==0)
                {
                    //插入u盘
                    QString USBDisk = QString(this->FirstDriveFromMask(lpdbv ->dbcv_unitmask))+":/";
                    mExternalDeviceList.append(USBDisk);
                    qDebug() << "存储设备插入: "<<USBDisk ;
                    qDebug()<<"###插入设备之后:";
                    for(int i=0;i<mExternalDeviceList.length();i++)
                    {
                        qDebug()<<mExternalDeviceList.at(i);
                    }
                }
            }
            break;
        case DBT_DEVICEREMOVECOMPLETE:
            if(lpdb->dbch_devicetype == DBT_DEVTYP_VOLUME)
            {
                PDEV_BROADCAST_VOLUME lpdbv = (PDEV_BROADCAST_VOLUME)lpdb;
                if(lpdbv->dbcv_flags == 0)
                {
                    QString USBDisk = QString(this->FirstDriveFromMask(lpdbv ->dbcv_unitmask))+":/";
                    for(int i=0;i<mExternalDeviceList.length();i++)
                    {
                        if(mExternalDeviceList.at(i)==USBDisk)
                            mExternalDeviceList.removeAt(i);
                    }
                    qDebug() << "存储设备移除: " <<USBDisk;

                    qDebug()<<"###拔出设备之后:";
                    for(int i=0;i<mExternalDeviceList.length();i++)
                    {
                        qDebug()<<mExternalDeviceList.at(i);
                    }
                }
            }
            break;
        }
    }
    return false;
}
#endif

void MainWindow::select_disk_slot(QString select)
{
    if(!select.isNull())
    {
        mOptationDisk.clear();
        mOptationDisk = select+"/";
        qDebug()<<"(SLOT)选择操作磁盘: "<<mOptationDisk;
#if 0
        QDesktopServices::openUrl(QUrl(mOptationDisk,QUrl::TolerantMode));
#endif
        //TODO
    }
}

void MainWindow::on_browseButton_clicked()
{
    ////    QString url = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    QString url;
#if 0
    if(HHT_PLATFORM_INFO)
        url = mProjectPath+"/";
    else
        url = QApplication::applicationDirPath()+tr("/hht-vctls/启动画面/");
#else
    if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
        url = QApplication::applicationDirPath()+tr("/hht-vctls/启动画面/");
    else
        url = mProjectPath+"/";
#endif
    if(!QDir(url).exists())
    {
        url = QApplication::applicationDirPath();
    }
    qDebug()<<"The Frist boot image Path: "<<url;
#if 1
    QString filePath = QFileDialog::getOpenFileName(this,tr("选择文件"),url,tr("Images(*.jpg)"));
    mFilePath = filePath;
    if(!filePath.isEmpty())
    {
        ui->logoLineEdit->clear();
        ui->logoLineEdit->setText(mFilePath);
    }
#else
    QFileDialog *dialog = new QFileDialog(this);
    dialog->setWindowTitle(tr("选择文件"));
    dialog->setDirectory(".");
    dialog->setNameFilter(tr("Images(*.png *.jpg)"));
    dialog->setFileMode(QFileDialog::ExistingFiles);
    dialog->setViewMode(QFileDialog::Detail);
    if(dialog->exec()==QDialog::Accepted)
    {
        QStringList filePathList =dialog->selectedFiles();
        QString filePath = filePathList.at(0);
        mFilePath = filePath;
        if(!filePath.isEmpty())
        {
            ui->logoLineEdit->clear();
            ui->logoLineEdit->setText(mFilePath);
        }
    }
#endif
}
void MainWindow::on_browseButton2_clicked()
{
    ////    QString url = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
    QString url;
#if 0
    if(HHT_PLATFORM_INFO)
        url = mProjectPath+"/";
    else
        url = QApplication::applicationDirPath()+tr("/hht-vctls/启动画面/");
#else
    if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
        url = QApplication::applicationDirPath()+tr("/hht-vctls/启动画面/");
    else
        url = mProjectPath+"/";
#endif
    if(!QDir(url).exists())
    {
        url = QApplication::applicationDirPath();
    }
    qDebug()<<"The Second Animation Path: "<<url;
#if 1
    QString filePath = QFileDialog::getOpenFileName(this,tr("选择文件"),url,tr("Animation(*.zip)"));
    mFilePath = filePath;
    if(!filePath.isEmpty())
    {
        ui->logoLineEdit2->clear();
        ui->logoLineEdit2->setText(mFilePath);
    }
#else
    QFileDialog *dialog = new QFileDialog(this);
    dialog->setWindowTitle(tr("选择文件"));
    dialog->setDirectory(".");
    dialog->setNameFilter(tr("Images(*.png *.jpg)"));
    dialog->setFileMode(QFileDialog::ExistingFiles);
    dialog->setViewMode(QFileDialog::Detail);
    if(dialog->exec()==QDialog::Accepted)
    {
        QStringList filePathList =dialog->selectedFiles();
        QString filePath = filePathList.at(0);
        mFilePath = filePath;
        if(!filePath.isEmpty())
        {
            ui->logoLineEdit2->clear();
            ui->logoLineEdit2->setText(mFilePath);
        }
    }
#endif
}

void MainWindow::on_outputbrosweButton_clicked()
{
    QString outputPath = QFileDialog::getExistingDirectory(this,tr("选择输出路径"),".", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(!outputPath.isEmpty())
    {
        ui->profileLineEdit->setText(outputPath+"");
    }
}

void MainWindow::on_pannelBox_currentTextChanged(const QString &arg1)
{
#if 0
    if(HHT_PLATFORM_INFO)
        mPanelFilePath = mProjectPath+"/"+arg1;
    else
        mPanelFilePath = mPanelPath+"/"+arg1;
#else
    if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
        mPanelFilePath = mPanelPath+"/"+arg1;
    else
         mPanelFilePath = mProjectPath+"/"+arg1;
#endif
    //    qDebug()<<"C###########"<<mPanelFilePath;
}

void MainWindow::on_generateButton_clicked()
{
    if(ui->profileLineEdit->text().isEmpty()/*|ui->logoLineEdit->text().isEmpty()|ui->logoLineEdit2->text().isEmpty()*/)
    {
        QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("请选择项目输出路径...."));
        QMessageBox::warning(NULL,"HHT-VCTL",text);
    }
    else
    {
        //TODO 1
        int count = ui->projectBox->currentText().length()-QString(".xml").length();
        QString projectName=ui->projectBox->currentText().mid(0,count);
        QString cachePath = mExcutePath+tr("/VCTL-%1").arg(projectName);
        qDebug()<<"///////////////////////////////"<<cachePath;
        bool result =WriteXMLContent(cachePath);
        //TODO 2
        if(result)
        {
            bool result1=false,result2=false,result3=false,result4=false;
            result1=copyFileToPath(mPanelFilePath,cachePath+"/"+ui->pannelBox->currentText(),true);
            result2=copyFileToPath(ui->logoLineEdit->text(),cachePath+"/boot0.jpg",true);
            result3=copyFileToPath(ui->logoLineEdit2->text(),cachePath+"/bootanimation.zip",true);
            if(result1/*&&result2&&result3*/)
            {
                //TODO 3打包zip 加密zip
                qDebug()<<"Start compress zip";
                //                result4 =JlCompress::compressDir(cachePath+".zip",cachePath);
                result4 = compressDirEncrypt(cachePath+tr(".zip"),cachePath,false);
                if(!result4)
                {
                    qDebug()<<"compress failed.";
                    QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("HHT-VCTL生成配置文件失败!!!"));
                    QMessageBox::about(NULL,"HHT-VCTL",text);
                }
                else
                {
                    qDebug()<<"compress success.";
                    QString destPath = ui->profileLineEdit->text();
                    QString destFilePath;
                    if(destPath.endsWith("/"))
                    {
                        destFilePath = destPath+tr("VCTL-%1.zip").arg(projectName);
                    }
                    else
                    {
#if 0
                        destFilePath = destPath+tr("/VCTL-%1_%2").arg(projectName).arg(ui->pannelBox->currentText());
#else
                        QString datetime = QDateTime::currentDateTime().toString("yyyyMMddhhmm");
                        QString str = ui->pannelBox->currentText();// PANEL_xxxxxxxxx.zip
#if 0
                        //PANEL_xxxxxxxxx_201810192250.zip
                        QString lastLabel = str.mid(0,str.length()-4)+tr("_%1.zip").arg(datetime);
#else
                        QString lastLabel;
                        if(isZipOutput) //配置文件后缀.zip/.config
                        {
                            lastLabel = str.mid(0,str.length()-4)+tr("_%1.zip").arg(datetime);
                        }
                        else
                        {
                            lastLabel = str.mid(0,str.length()-4)+tr("_%1.config").arg(datetime);
                        }
#endif
                        destFilePath = destPath+tr("/VCTL-%1_%2").arg(projectName).arg(lastLabel);
#endif
                    }
                    bool res=copyFileToPath(cachePath+".zip",destFilePath,true);
                    if(res)
                    {
                        qDebug()<<"Generate profile success.";
                        QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("HHT-VCTL生成配置文件成功..."));
                        QMessageBox::about(NULL,"HHT-VCTL",text);
                    }
                    else
                    {
                        qDebug()<<"Generate profile failed.";
                        QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("HHT-VCTL生成配置文件失败.!!!"));
                        QMessageBox::about(NULL,"HHT-VCTL",text);
                    }
                }
            }
        }
        //TODO4 拷贝zip 删除原始zip以及文件夹
        bool result5=false;bool result6=false;
        result5 = delDir(cachePath);
        if(!result5)
        {
            qDebug()<<"dell dir failed.";
        }
        QFile zipFile(cachePath+".zip");
        if(zipFile.exists())
        {
            result6 = zipFile.remove();
            if(!result6)
                qDebug()<<"del zip file failed.";
        }
    }
}

void MainWindow::on_pannelButton_clicked()
{
    QString url;
#if 0
    if(HHT_PLATFORM_INFO)
        url = mProjectPath+"/";
    else
        url = QApplication::applicationDirPath()+tr("/hht-vctls/屏参/");
#else
    if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
        url = QApplication::applicationDirPath()+tr("/hht-vctls/屏参/");
    else
        url = mProjectPath+"/";
#endif
    if(!QDir(url).exists())
    {
        url = QApplication::applicationDirPath();
    }
    qDebug()<<"The Pannel Select Path: "<<url;

    QString filePath = QFileDialog::getOpenFileName(this,tr("选择文件"),url,tr("ZIP(*.zip)"));
    if(!filePath.isEmpty())
    {
        QFileInfo info(filePath);
        QString fileName = info.fileName();
        qDebug()<<"/////////"<<fileName.mid(0,5);
        bool isExist = false;
        if(fileName.mid(0,5)==tr("PANEL")) //判断导入目标是否合法
        {
            for(int i=0;i<ui->pannelBox->count();i++) //导入目标是否已经存在
            {
                if(fileName==ui->pannelBox->itemText(i))
                {
                    isExist = true;
                }
            }
            if(!isExist)
            {
                //TODO将文件拷贝到 :HHT-VCTL/hht-vctls/屏参/
                bool result =false;
#if 0
                if(HHT_PLATFORM_INFO)
                    result =copyFileToPath(filePath,mProjectPath+"/"+fileName,true);
                else
                    result =copyFileToPath(filePath,mPanelPath+"/"+fileName,true);
#else
              if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
                  result =copyFileToPath(filePath,mPanelPath+"/"+fileName,true);
              else
                  result =copyFileToPath(filePath,mProjectPath+"/"+fileName,true);
#endif
                if(!result)
                {
                    qDebug()<<"copy file failed.";
                }
                else
                {
                    ui->pannelBox->addItem(fileName);
                }
            }
            else
            {
                qDebug()<<"Import zip is already int the list.";
                QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("导入\"%1\" PANEL文件已经存在列表中....").arg(fileName));
                QMessageBox::warning(NULL,"HHT-VCTL",text);
            }
        }
        else {
            qDebug()<<"Import zip not illegill.";
            QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("导入ZIP文件名必须以\"%1\"开始....").arg("PANEL"));
            QMessageBox::warning(NULL,"HHT-VCTL",text);
        }
    }
}

void MainWindow::on_projectButton_clicked()
{
    QString url;
#if 0
    if(HHT_PLATFORM_INFO)
        url = mProjectPath+"/";
    else
        url = QApplication::applicationDirPath()+tr("/hht-vctls/项目/");
#else
     if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
         url = QApplication::applicationDirPath()+tr("/hht-vctls/项目/");
     else
         url = mProjectPath+"/";
#endif
    if(!QDir(url).exists())
    {
        url = QApplication::applicationDirPath();
    }
    qDebug()<<"The Project Select Path: "<<url;
    QString filePath = QFileDialog::getOpenFileName(this,tr("选择文件"),url,tr("XML(*.xml)"));
    if(!filePath.isEmpty())
    {
        bool isExist =false;
        QFileInfo info(filePath);
        QString fileName = info.fileName();
        qDebug()<<"/////////"<<fileName.mid(0,3);
        QString itemName;
        if(fileName.mid(0,3)==tr("HHT"))
        {
            for(int i=0;i<ui->projectBox->count();i++) //导入目标是否已经存在
            {
                if(fileName ==ui->projectBox->itemText(i))
                {
                    isExist = true;
                }
            }
            if(!isExist)
            {
                //TODO将文件拷贝到 :HHT-VCTL/hht-vctls/项目/HHT_828.7
                bool result=false;
 #if 0
                if(HHT_PLATFORM_INFO)
                    result =copyFileToPath(filePath,mProjectPath+"/"+fileName,true);
                else
                    result=copyFileToPath(filePath,mDevicePath+"/"+fileName,true);
#else
                if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
                    result=copyFileToPath(filePath,mDevicePath+"/"+fileName,true);
                else
                    result =copyFileToPath(filePath,mProjectPath+"/"+fileName,true);
#endif
                itemName = info.fileName();
                if(!result)
                {
                    qDebug()<<"copy file failed.";
                }
                else
                {
                    ui->projectBox->addItem(itemName);
                }
            }
            else
            {
                qDebug()<<"Import device is already int the list.";
                QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(QString("导入\"%1\"项目配置文件已经存在列表中....").arg(fileName));
                QMessageBox::warning(NULL,"HHT-VCTL",text);
            }
        }
        else {
            qDebug()<<"Import xml not illegill.";
            QString text = QString("<font face='仿宋' size='5' bold='true' color='black'>%1</font>").arg(tr("导入XML项目配置文件名必须以\"%1\"开始....").arg("HHT"));
            QMessageBox::warning(NULL,"HHT-VCTL",text);
        }
    }
}

void MainWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    mProjectPath=mConfiguartionPath+"/"+arg1;
    qDebug()<<"##"<<mProjectPath;
    on_actionrefresh_triggered();
}

void MainWindow::on_projectBox_currentTextChanged(const QString &arg1)
{
#if 0
    if(HHT_PLATFORM_INFO)
    {
        mDeviceFilePath = mProjectPath+"/"+arg1;
        ui->treeWidget->clear();
        ParseXML(mDeviceFilePath);
    }
    else
    {
        mDeviceFilePath= mDevicePath+"/"+arg1;
        ui->treeWidget->clear();
        ParseXML(mDeviceFilePath);
        //    qDebug()<<"2C###########"<<mDeviceFilePath;
    }
#else
    if(HHT_PLATFORM_INFO==PLATFORM_DEFAULT)
    {
        mDeviceFilePath= mDevicePath+"/"+arg1;
        ui->treeWidget->clear();
        ParseXML(mDeviceFilePath);
        //    qDebug()<<"2C###########"<<mDeviceFilePath;
    }
    else {
        mDeviceFilePath = mProjectPath+"/"+arg1;
        ui->treeWidget->clear();
        ParseXML(mDeviceFilePath);
    }
#endif
}

void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    if(item->checkState(0)==Qt::Checked&&(item->text(2)==tr("true")))
    {
        item->setCheckState(0,Qt::Unchecked);
        item->setText(2,tr("false"));
    }
    else if(item->checkState(0)==Qt::Unchecked&&(item->text(2)==tr("false")))
    {
        item->setCheckState(0,Qt::Checked);
        item->setText(2,tr("true"));
    }
    else
    {
        qDebug()<<"Click nothing to be changed.";
    }
    ui->treeWidget->update();
}

void MainWindow::on_actionoutput_triggered()
{
    isZipOutput = !isZipOutput;
    if(isZipOutput)
    {
        ui->actionoutput->setText(tr("输出(.zip)"));
    }
    else
    {
        ui->actionoutput->setText(tr("输出(.config)"));
    }
}
