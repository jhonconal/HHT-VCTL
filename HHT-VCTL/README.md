# HHT-VCTL-Master

HHT 版控配置文件
Version: 1.18.11.21
Update: Nov 21 2018 13:11:13 build
注意：
1.该版本兼容通用平台以及MSDA828.15特殊平台
2.程序兼容由hht-config.ini文件决定，存在该配置参数即界面切换至MSD828.15平台即以项目(ABIMAGE GENEE HANSUNG JECTOR OEM PREDIA RS VNXJECTOR)区分,切换至通用平台删除hht-config.ini即可
3.hht-config.ini 设置两个参数(PLATFORM PROJECTS):平台属性参数以及hht-vctls目录下需要被索引在程序中的项目名称