#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QDir>
#ifdef Q_OS_WIN32
#include <windows.h>
#include <winbase.h>
#include <winnls.h>
#include <dbt.h>
#endif
#include <QMessageBox>
#include <QFont>
#include <QColor>
#include <QListView>
#include <QFileDialog>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QDomDocument>
#include <QStandardItemModel>
#include <QTreeWidgetItem>
#include <QCryptographicHash>

#include "JlCompress.h"
#include "getusbdiskwidget.h"

#define MAJOR_VERSION ("1.")
#define ENCRYPT_PASSWD ("hht123456")  //加密密码
#define HHT_PROPERTY_VISABLE   0

enum PlatformInfo
{
    PLATFORM_DEFAULT =0,
    PLATFORM_MSD828_15,
    PLATFORM_MSD848_15,
};
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void initWnd();
    void initVariables();

    char *GetBuildDate();
    QString GetBuildVersion();

    bool CompareList(QStringList src,QStringList dest);
    quint64 dirFileSize(const QString &path);

    void quazip_test();
    bool compressDirEncrypt(QString fileCompressed,QString dir,bool isEncrypt);

    QString GenerateJSon();
    void AnalyticsJSon(QString jsonPath);

    void ParseXML(QString xmlFilePath);

    bool WriteXMLContent(QString updatePropPath); //将XML文件挑选属性写成text文件

    QStringList getFileNames(const QString &path,const QStringList &nameFilters);
    QStringList getFileNames(const QString &path);

    //
    bool copyFileToPath(QString sourceDir ,QString toDir, bool coverFileIfExist);
    bool copyDirectoryFiles(const QString &fromDir, const QString &toDir, bool coverFileIfExist);
    bool delDir(const QString &path);

private slots:
    void on_actionopen_triggered();
    void on_actionsave_triggered();
    void on_actionsave_as_triggered();
    void on_actionabout_triggered();
    void on_actionrefresh_triggered();
    void on_actionoutput_triggered();

    void on_browseButton_clicked();

    void on_browseButton2_clicked();

    void on_outputbrosweButton_clicked();

    void on_pannelBox_currentTextChanged(const QString &arg1);

    void on_generateButton_clicked();

    void on_pannelButton_clicked();

    void on_projectButton_clicked();

    void on_projectBox_currentTextChanged(const QString &arg1);

    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_comboBox_currentTextChanged(const QString &arg1);

protected:
#if 0
    //#ifdef Q_OS_WIN32
    char FirstDriveFromMask(ULONG unitmask);
    bool nativeEvent(const QByteArray & eventType, void * message, long*result);
#endif
public slots:
    void  select_disk_slot(QString select);
private:
    Ui::MainWindow *ui;
    bool isZipOutput;
    QString  mExcutePath ,mConfiguartionPath,mPanelPath,mDevicePath;//执行程序地址HOME ,面板配置文件夹，设备代号文件夹
    QString  mProjectPath; //828.15 配置项目路径
    QString  mPanelFilePath,mDeviceFilePath;
    QStringList mExternalDeviceList;
    QString       mOptationDisk;//选择操作external disk
    GetUsbDiskWidget *mGetUsbDiskWidget;
    QString mAbout;
    QString mFilePath;
    QStringList mProjectList,mPannelList;
    int mNodeListCount;
};

#endif // MAINWINDOW_H
