#include "mainwindow.h"
#include <QApplication>
#include <QStringList>
#include <QSettings>
extern int HHT_PLATFORM_INFO;
extern QStringList  gProgectList;
extern QString        gPlatfrom;
void  read_hht_configs(QString configPath);
int main(int argc, char *argv[])
{
#if (QT_VERSION >= QT_VERSION_CHECK(5,6,0))
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);//Hight DPI support
#endif
    QApplication a(argc, argv);
    //读取配置文件判断属性
    QString url;
    url = QApplication::applicationDirPath()+QString("/hht-configs.ini");
    read_hht_configs(url);

    MainWindow w;
    w.show();

    return a.exec();
}

void read_hht_configs(QString configPath)
{
    qDebug()<<Q_FUNC_INFO;
    QFile config(configPath);
    if(config.exists())
    {
        QSettings settings(configPath,QSettings::IniFormat);
        if(settings.contains("SETTINGS/PROJECTS"))
        {
            QString projects = settings.value("SETTINGS/PROJECTS").toString();
            qDebug()<<"PROJECTS= "<<projects;
            gProgectList = projects.split('#');
            for(int i=0;i<gProgectList.count();i++)
                qDebug()<<gProgectList.at(i);
        }
        if(settings.contains("SETTINGS/PLATFORM"))
        {
            QString value = settings.value("SETTINGS/PLATFORM").toString();
            gPlatfrom = value;
            qDebug()<<"PLATFORM= "<<value;
#if 0
            if(value=="MSD828.15")
            {
                HHT_PLATFORM_INFO = PLATFORM_MSD828_15;
                qDebug()<<"platforms= "<<value;
            }
            else if (value=="MSD848.15") {
                HHT_PLATFORM_INFO = PLATFORM_MSD848_15;
                qDebug()<<"platforms= "<<value;
            }
            else
            {
                HHT_PLATFORM_INFO = PLATFORM_DEFAULT;
                qDebug()<<"[1]platforms= default";
            }
#else
           if(gPlatfrom.isNull()||gPlatfrom==nullptr||gPlatfrom=="")
           {
               HHT_PLATFORM_INFO = PLATFORM_DEFAULT;
           }
#endif
        }
        else
        {
            HHT_PLATFORM_INFO =PLATFORM_DEFAULT;
            qDebug()<<"[2]platforms= default";
        }
    }
    else
    {
        HHT_PLATFORM_INFO=PLATFORM_DEFAULT;
        qDebug()<<"[3]platforms= default";
    }
}
